import utfpr.ct.dainf.if62c.pratica.Jogador;
import utfpr.ct.dainf.if62c.pratica.JogadorComparator;
import utfpr.ct.dainf.if62c.pratica.Time;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * 
 * Template de projeto de programa Java usando Maven.
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
public class Pratica62 {
    public static void main(String[] args) {
        Jogador wilson = new Jogador(84, "Wilson");
        Jogador dodo = new Jogador(98, "Dodô");
        Jogador marcio = new Jogador(76, "Dodô");
        Jogador werley = new Jogador(66, "Werley");
        Jogador william = new Jogador(66, "William Matheus");
        Jogador alan = new Jogador(5, "Alan santos");
        Jogador matheus = new Jogador(20, "Matheus Galdezani");
        Jogador tomas = new Jogador(26, "Tomas Bastos");
        Jogador neto = new Jogador(9, "Neto Berola");
        Jogador henrique = new Jogador(91, "Henrique Almeida");
        Jogador kleber = new Jogador(83, "Kleber");
        Time time1 = new Time();
        time1.addJogador("Goleiro", wilson);
        time1.addJogador("Zagueiro central", marcio);
        time1.addJogador("Quarto zagueiro", werley);
        time1.addJogador("Lateral direito", dodo);
        time1.addJogador("Lateral esquerdo", william);
        time1.addJogador("Primeiro volante", alan);
        time1.addJogador("Segundo volante", matheus);
        time1.addJogador("Meia armador", tomas);
        time1.addJogador("Meia atacante", neto);
        time1.addJogador("Atacante", henrique);
        time1.addJogador("Centroavante", kleber);
        
        Time time2 = new Time();
        Jogador weverton = new Jogador(12, "Weverton");
        Jogador jonathan = new Jogador(2, "Jonathan");
        Jogador paulo = new Jogador(13, "Paulo André");
        Jogador thiago = new Jogador(44, "Thiago Heleno");
        Jogador sidcley = new Jogador(8, "Sidcley");
        Jogador otavio = new Jogador(7, "Otávio");
        Jogador rossetto = new Jogador(20, "Matheus Rossetto");
        Jogador guilherme = new Jogador(17, "Guilherme");
        Jogador nikao = new Jogador (11, "Nikão");
        Jogador douglas = new Jogador(77, "Douglas Coutinho");
        Jogador grafite = new Jogador(23, "Grafite");
        time2.addJogador("Goleiro", weverton);
        time2.addJogador("Zagueiro central", paulo);
        time2.addJogador("Quarto zagueiro", thiago);
        time2.addJogador("Lateral direito", jonathan);
        time2.addJogador("Lateral esquerdo", sidcley);
        time2.addJogador("Primeiro volante", otavio);
        time2.addJogador("Segundo volante", rossetto);
        time2.addJogador("Meia armador", guilherme);
        time2.addJogador("Meia atacante", nikao);
        time2.addJogador("Atacante", douglas);
        time2.addJogador("Centroavante", grafite);
        System.out.println("Posicao \t Time 1 \t Time 2");
        JogadorComparator joga = new JogadorComparator(false, true, false);

    }
}
